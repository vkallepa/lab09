#!/usr/bin/env python

import cgitb
import cgi
import pymysql
cgitb.enable()
form = cgi.FieldStorage()
fullName = str(form.getvalue('fullName'))
print("Content-Type:text/html;charset=utf-8")
print()

print("<html>")
print("<head>")
print("""<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">""")
print("<title>lab09</title>")
print("</head>")
print("<body>")
print("""  
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="http://localhost/mainpage.html">Lab09</a>
        <div class="collapse navbar-collapse" >
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="http://localhost/mainpage.html">Add Students</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost/deletepage.html">Delete Students</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost/records.py">All Students records</a>
            </li>
          </ul>
        </div>
    </nav>


 """)
print("<div class='container'>")
print("<h2>sucessfully deleted {}'s record </h2>".format(fullName))
print("</div>")
print("</body>")
print("</html>")



sql_conn = pymysql.connect(db='student_grades',user='root',passwd='password',host='localhost')
sql_conn_cursor = sql_conn.cursor()
sql_conn_cursor.execute("delete from student_grades where s_name = '{}'".format(fullName))
sql_conn.commit()
