#!/usr/bin/env python

import cgitb
import cgi
import pymysql
cgitb.enable()
sql_conn = pymysql.connect(db='student_grades',user='root',passwd='password',host='localhost')
sql_conn_cursor = sql_conn.cursor()
sql_conn_cursor.execute("select * from student_grades")
data = sql_conn_cursor.fetchall()

print("Content-Type:text/html;charset=utf-8")
print()
print("<html>")
print("<head>")
print("""<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">""")
print("<style>")
print("table, th, td {")
print("border:1px solid black;")
print("}")
print("</style>")
print("<title>lab09</title>")
print("</head>")
print("<body>")
print("""  
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="http://localhost/mainpage.html">Lab09</a>
        <div class="collapse navbar-collapse" >
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="http://localhost/mainpage.html">Add Students</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost/deletepage.html">Delete Students</a>
            </li>
          </ul>
        </div>
    </nav>


 """)
print("<div class='container mt-3'>")
print("<table style='width:100%'>")
print("<tr>")
print("<th>Full Name</th>")
print("<th>Average Score</th>")
print("</tr>")
for eachuser in data:
    print("<tr>")
    print("<td>{}</td>".format(eachuser[0]))
    print("<td>{}</td>".format(int(eachuser[1] + eachuser[2] + 2*eachuser[3]) / int(4)))
    print("</tr>")

print("</table>")
print("</div>")
print("</body>")
print("</html>")


